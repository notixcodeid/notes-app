<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Note;

class NoteController extends Controller
{
    public function index()
    {
        $notes = Note::all(); // Ambil semua catatan dari database

        if (request()->wantsJson()) {
            return response()->json($notes);
        }

        return view('notes.index', compact('notes'));
    }

    public function create()
    {
        $this->middleware('checkUserRole');

        // Periksa tipe permintaan, jika permintaan adalah JSON, langsung arahkan ke store
        if (request()->wantsJson()) {
            return $this->store(request());
        }

        return view('notes.create');
    }

    public function store(Request $request)
    {
        // Validasi dan simpan catatan baru
        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'content' => 'required|string',
        ]);

        $note = new Note();
        $note->title = $validatedData['title'];
        $note->content = $validatedData['content'];
        $note->user_id = auth()->user()->id; // Asumsi user_id adalah kolom yang menunjukkan pemilik catatan
        $note->save();

        // Data yang disimpan
        $savedNote = [
            'id' => $note->id,
            'title' => $note->title,
            'content' => $note->content,
            'user_id' => $note->user_id,
        ];

        // Jika permintaan ingin respons JSON (misalnya, saat menggunakan API),
        // Anda dapat mengembalikan respons JSON sebagai berikut:
        if ($request->wantsJson()) {
            return response()->json(['message' => 'Catatan berhasil disimpan', 'data' => $savedNote], 201);
        }

        // Jika permintaan tidak menginginkan respons JSON, maka Anda dapat
        // mengarahkan pengguna kembali ke tampilan dengan pesan sukses:
        return redirect()->route('notes.index')->with('success', 'Catatan berhasil disimpan.');
    }

    public function show(Note $note, Request $request)
    {
        if ($request->wantsJson()) {
            return response()->json($note);
        }

        return view('notes.show', compact('note'));
    }

    public function edit(Note $note)
    {
        $this->middleware('checkUserRole');

        // Periksa tipe permintaan, jika permintaan adalah JSON, langsung arahkan ke update
        if (request()->wantsJson()) {
            return $this->update(request());
        } 

        return view('notes.edit', compact('note'));
    }

    public function update(Request $request, Note $note)
    {
        $this->middleware('checkUserRole');
        // Validasi dan update catatan
        // Validasi data yang diterima dari form
        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'content' => 'required|string',
        ]);

        // Perbarui catatan berdasarkan data yang valid
        $note->title = $validatedData['title'];
        $note->content = $validatedData['content'];
        $note->save();

        $updatedNote = [
            'id' => $note->id,
            'title' => $note->title,
            'content' => $note->content,
            'user_id' => $note->user_id,
        ];

        if ($request->wantsJson()) {
            return response()->json(['message' => 'Catatan berhasil diperbarui', 'data' => $updatedNote], 200);
        }

        return redirect()->route('notes.index')->with('success', 'Catatan berhasil diperbarui.');
    }

    public function destroy(Note $note, Request $request)
    {
        $this->middleware('checkUserAdmin');
        // Hapus catatan
        $note->delete();

        if ($request->wantsJson()) {
            return response()->json(['message' => 'Catatan berhasil dihapus'], 201);
        }

        return redirect()->route('notes.index')->with('success', 'Catatan berhasil dihapus.');
    }

}
