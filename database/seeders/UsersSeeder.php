<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Tambahkan pengguna biasa
        User::create([
            'name' => 'User Biasa',
            'email' => 'user@example.com',
            'password' => Hash::make('password'), // Ganti dengan password yang sesuai
            'role' => 'user',
        ]);

        // Tambahkan pengguna editor
        User::create([
            'name' => 'User Editor',
            'email' => 'editor@example.com',
            'password' => Hash::make('password'), // Ganti dengan password yang sesuai
            'role' => 'editor',
        ]);

        // Tambahkan admin
        User::create([
            'name' => 'Admin',
            'email' => 'admin@example.com',
            'password' => Hash::make('password'), // Ganti dengan password yang sesuai
            'role' => 'admin',
        ]);
    }
}
