<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\NoteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/token', function () {
    return csrf_token(); 
})->middleware('auth')->middleware('throttle:10,1'); // Ini membuat token CSRF kadaluarsa setelah 10 request dalam 1 menit

Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register');

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');

Auth::routes();
Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('auth')->name('dashboard');
Route::resource('notes', NoteController::class);
Route::get('/notes', [NoteController::class, 'index'])->middleware('auth')->name('notes.index');
Route::get('/notes/{id}', [NoteController::class, 'show'])->middleware('auth')->name('notes.show');
Route::post('/notes/create', [NoteController::class, 'create'])->middleware('checkUserRole')->name('notes.create');
Route::put('/notes/{id}', [NoteController::class, 'update'])->middleware('checkUserRole')->name('notes.update');
Route::delete('/notes/{id}', [NoteController::class, 'destroy'])->middleware('checkUserAdmin')->name('notes.destroy');

// Route::group(['middleware' => 'admin'], function () {
//     Route::post('/notes/create', [NoteController::class, 'create'])->name('notes.create');
//     Route::put('/notes/{id}', [NoteController::class, 'update'])->name('notes.update');
//     Route::delete('/notes/{id}', [NoteController::class, 'destroy'])->name('notes.destroy');
//     // Rute-rute yang hanya dapat diakses oleh admin
// });

// Route::group(['middleware' => 'editor'], function () {
//     Route::post('/notes/create', [NoteController::class, 'create'])->name('notes.create');
//     Route::put('/notes/{id}', [NoteController::class, 'update'])->name('notes.update');
//     // Rute-rute yang hanya dapat diakses oleh editor
// });

// Route::group(['middleware' => 'user'], function () {
//     // Rute-rute yang dapat diakses oleh pengguna biasa
// });

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
