@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@if(auth()->user()->role === 'user')
        <!-- Konten untuk Pengguna Biasa -->
@elseif(auth()->user()->role === 'editor')
        <!-- Konten untuk Editor -->
@elseif(auth()->user()->role === 'admin')
        <!-- Konten untuk Admin -->
@endif
@endsection
