@extends('layouts.app')

@section('content')
@if(auth()->user()->role === 'user')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('sidebar') <!-- Sertakan sidebar di sini -->
        </div>
        <div class="col-md-10">
            <div class="d-flex justify-content-between align-items-center">
                <h1>Notes List</h1>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Content</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($notes as $note)
                        <tr>
                            <th scope="row">{{ $note->id }}</th>
                            <td>{{ $note->title }}</td>
                            <td>{{ $note->content }}</td>
                            <td>
                                <a href="{{ route('notes.show', $note) }}" class="btn btn-info">Show</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
    <!-- Konten untuk Pengguna Biasa -->
@elseif(auth()->user()->role === 'editor')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('sidebar') <!-- Sertakan sidebar di sini -->
        </div>
        <div class="col-md-10">
            <div class="d-flex justify-content-between align-items-center">
                <h1>Notes List</h1>
                <a href="{{ route('notes.create') }}" class="btn btn-success">Create</a>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Content</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($notes as $note)
                        <tr>
                            <th scope="row">{{ $note->id }}</th>
                            <td>{{ $note->title }}</td>
                            <td>{{ $note->content }}</td>
                            <td>
                                <a href="{{ route('notes.show', $note) }}" class="btn btn-info">Show</a>
                                <a href="{{ route('notes.edit', $note) }}" class="btn btn-primary">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
    <!-- Konten untuk Editor -->
@elseif(auth()->user()->role === 'admin')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('sidebar') <!-- Sertakan sidebar di sini -->
        </div>
        <div class="col-md-10">
            <div class="d-flex justify-content-between align-items-center">
                <h1>Notes List</h1>
                <a href="{{ route('notes.create') }}" class="btn btn-success">Create</a>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Content</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($notes as $note)
                        <tr>
                            <th scope="row">{{ $note->id }}</th>
                            <td>{{ $note->title }}</td>
                            <td>{{ $note->content }}</td>
                            <td>
                                <a href="{{ route('notes.show', $note) }}" class="btn btn-info">Show</a>
                                <a href="{{ route('notes.edit', $note) }}" class="btn btn-primary">Edit</a>
                                <form action="{{ route('notes.destroy', $note->id) }}" method="POST" style="display: inline;">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus catatan ini?')">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
    <!-- Konten untuk Admin -->
@endif
@endsection