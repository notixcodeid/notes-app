@extends('layouts.app')

@section('content')
@if(auth()->user()->role === 'user')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                @include('sidebar') <!-- Sertakan sidebar di sini -->
            </div>
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        Akses Ditolak
                    </div>
                    <div class="card-body">
                        <h5>Maaf, anda tidak memiliki akses di sini.</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Konten untuk Pengguna Biasa -->
@elseif(auth()->user()->role === 'editor')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                @include('sidebar') <!-- Sertakan sidebar di sini -->
            </div>
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        Edit Note: {{ $note->title }}
                    </div>
                    <div class="card-body">
                        <form action="{{ route('notes.update', $note) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <label for="title" class="form-label">Title</label>
                                <input type="text" class="form-control" id="title" name="title" value="{{ $note->title }}" required>
                            </div>
                            <div class="mb-3">
                                <label for="content" class="form-label">Content</label>
                                <textarea class="form-control" id="content" name="content" rows="4" required>{{ $note->content }}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
                <a href="{{ route('notes.index') }}" class="btn btn-secondary mt-3">Back</a>
            </div>
        </div>
    </div>
        <!-- Konten untuk Editor -->
@elseif(auth()->user()->role === 'admin')
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                @include('sidebar') <!-- Sertakan sidebar di sini -->
            </div>
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        Edit Note: {{ $note->title }}
                    </div>
                    <div class="card-body">
                        <form action="{{ route('notes.update', $note->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <label for="title" class="form-label">Title</label>
                                <input type="text" class="form-control" id="title" name="title" value="{{ $note->title }}" required>
                            </div>
                            <div class="mb-3">
                                <label for="content" class="form-label">Content</label>
                                <textarea class="form-control" id="content" name="content" rows="4" required>{{ $note->content }}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
                <a href="{{ route('notes.index') }}" class="btn btn-secondary mt-3">Back</a>
            </div>
        </div>
    </div>
        <!-- Konten untuk Admin -->
@endif
@endsection