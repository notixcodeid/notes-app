@extends('layouts.app')

@section('content')
@if(auth()->user()->role === 'user')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('sidebar') <!-- Sertakan sidebar di sini -->
        </div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Note: {{ $note->title }}
                </div>
                <div class="card-body">
                    <h5 class="card-title">Title: {{ $note->title }}</h5>
                    <p class="card-text">Content: {{ $note->content }}</p>
                </div>
                </div>
                <a href="{{ route('notes.index') }}" class="btn btn-primary mt-3">Back</a>
            </div>
        </div>
    </div>
</div>
        <!-- Konten untuk Pengguna Biasa -->
@elseif(auth()->user()->role === 'editor')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('sidebar') <!-- Sertakan sidebar di sini -->
        </div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Note: {{ $note->title }}
                </div>
                <div class="card-body">
                    <h5 class="card-title">Title: {{ $note->title }}</h5>
                    <p class="card-text">Content: {{ $note->content }}</p>
                </div>
                </div>
                <a href="{{ route('notes.index') }}" class="btn btn-primary mt-3">Back</a>
            </div>
        </div>
    </div>
</div>
        <!-- Konten untuk Editor -->
@elseif(auth()->user()->role === 'admin')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('sidebar') <!-- Sertakan sidebar di sini -->
        </div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Note: {{ $note->title }}
                </div>
                <div class="card-body">
                    <h5 class="card-title">Title: {{ $note->title }}</h5>
                    <p class="card-text">Content: {{ $note->content }}</p>
                </div>
                </div>
                <a href="{{ route('notes.index') }}" class="btn btn-primary mt-3">Back</a>
            </div>
        </div>
    </div>
</div>
        <!-- Konten untuk Admin -->
@endif
@endsection
