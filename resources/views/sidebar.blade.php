<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body>

<div class="w3-sidebar w3-bar-block w3-light-grey w3-card" style="width:160px;">
  <a href="{{ url('/dashboard') }}" class="w3-bar-item w3-button">Dashboard</a>
  <button class="w3-button w3-block w3-left-align" onclick="myAccFunc()">
  Workspace <i class="fa fa-caret-down"></i>
  </button>
  <div id="demoAcc" class="w3-hide w3-white w3-card">
    <a href="{{ route('notes.create') }}" class="w3-bar-item w3-button">Create</a>
    <a href="{{ url('/notes') }}" class="w3-bar-item w3-button">Notes</a>
  </div>
</div>

<script>
function myAccFunc() {
  var x = document.getElementById("demoAcc");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
    x.previousElementSibling.className += " w3-white";
  } else { 
    x.className = x.className.replace(" w3-show", "");
    x.previousElementSibling.className = 
    x.previousElementSibling.className.replace(" w3-white", "");
  }
}
</script>

</body>
</html>
