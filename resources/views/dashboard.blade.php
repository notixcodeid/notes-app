@extends('layouts.app')

@section('content')
@if(auth()->user()->role === 'user')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('sidebar') <!-- Sertakan sidebar di sini -->
        </div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('[USER] You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- Konten untuk Pengguna Biasa -->
@elseif(auth()->user()->role === 'editor')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('sidebar') <!-- Sertakan sidebar di sini -->
        </div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('[USER] You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- Konten untuk Editor -->
@elseif(auth()->user()->role === 'admin')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('sidebar') <!-- Sertakan sidebar di sini -->
        </div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('[USER] You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- Konten untuk Admin -->
@endif
@endsection
